//
//  ListViewVC.m
//  Q'd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "ListViewVC.h"
#import "ListViewCell.h"
#import "DetailBistroVC.h"
#import <UIViewController+MMDrawerController.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>

#import "Common.h"
#import "MBProgressHUD.h"
#import <UIImageView+WebCache.h>

@interface ListViewVC ()
{
    IBOutlet UITableView *tblView;
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet MKMapView *mapView;
    __weak IBOutlet MKMapView *mapViewFullScreen;
    BOOL isFiltered;
    __weak IBOutlet UIButton *mapViewBtn;
    __weak IBOutlet UIImageView *fadeImgView;
    BOOL showFullMap;
    
    NSMutableArray *venuesArray;
    CLLocationManager *locationManager;
    int pageOffset;
    BOOL isPageRefresing;
    BOOL isMorePage;
    CLLocationCoordinate2D coord;

    BOOL isSearchClicked;
    int searchPageOffset;
}
@end

@implementation ListViewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    [mapView addGestureRecognizer:tap];
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    
    [mapViewFullScreen addGestureRecognizer:tap1];
    
    // Notification For Fade ImgView
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showFadeImg:) name:@"ShowFadeImg" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideFadeImg:) name:@"HideFadeImg" object:nil];

    // Using the drawer
    
    venuesArray = [[NSMutableArray alloc]init];

    [venuesArray removeAllObjects];
    
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    [locationManager requestAlwaysAuthorization];
   
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];

    MKCoordinateRegion region = mapView.region;
    
    CLLocation *location = [locationManager location];
    coord.longitude = location.coordinate.longitude;
    coord.latitude = location.coordinate.latitude;
    
    region.center = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
    region.span.longitudeDelta /= 500.0; // Bigger the value, closer the map view
    region.span.latitudeDelta /= 500.0;
    [mapView setRegion:region animated:YES]; // Choose if you want animate or not
    [mapViewFullScreen setRegion:region animated:YES];
    pageOffset = 1;

    // For Search
    isSearchClicked = NO;
    
    [self VenuesAPI];
}

-(void)dismissKeyboard1 {
    [searchBar resignFirstResponder];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [fadeImgView setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

#pragma mark - VenuesAPI

-(void)VenuesAPI
{
    
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"latitude"]] forKey:@"latitude"];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"longitude"]] forKey:@"longitude"];
    [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"]] forKey:@"user_id"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/users/getvenues/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
//            venuesArray = [response valueForKey:@"venues"];
            [venuesArray addObjectsFromArray:[response valueForKey:@"venues"]];
            
//            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            
            NSMutableArray *arrCoordinateStr = [[NSMutableArray alloc] initWithCapacity:venuesArray.count];
            
            [arrCoordinateStr addObject:@"12.970760345459, 80.2190093994141"];
            [arrCoordinateStr addObject:@"12.9752297537231, 80.2313079833984"];
            [arrCoordinateStr addObject:@"12.9788103103638, 80.2412414550781"];
            
            [mapView removeAnnotations:mapView.annotations];
            [mapViewFullScreen removeAnnotations:mapViewFullScreen.annotations];

            for(int i = 0; i < venuesArray.count; i++)
            {
                [self addPinWithTitle:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"name"]] AndLatitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"latitude"]] AndLongitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"longitude"]]];
            }
            
            NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
            
            if ([[response valueForKey:@"venues_count"] intValue]%5 >= 0) {
                isMorePage=YES;
            }
            else
            {
                isMorePage=NO;
            }
            
            [tblView reloadData];
            isPageRefresing=NO;
            
        }
        else
        {
            
        }
    }];

}

#pragma mark - AddMultiplePins

-(void)addPinWithTitle:(NSString *)title AndLatitude:(NSString *)strLatitude AndLongitude:(NSString*)strLongitude
{
    MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
    
//    // clear out any white space
//    strCoordinate = [strCoordinate stringByReplacingOccurrencesOfString:@" " withString:@""];
//    
//    // convert string into actual latitude and longitude values
//    NSArray *components = [strCoordinate componentsSeparatedByString:@","];
    
    double latitude = [strLatitude doubleValue];
    double longitude = [strLongitude doubleValue];
    
    // setup the map pin with all data and add to map view
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    mapPin.title = title;
    mapPin.coordinate = coordinate;
    
//    static NSString *reuseId = @"asdf";
//    
//    MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: reuseId];
//    if (pin == nil)
//        pin = [[MKPinAnnotationView alloc] initWithAnnotation:mapPin reuseIdentifier: reuseId];
//    else
//        pin.annotation = mapPin;
//    
////    pin.annotation = mapPin;
//    pin.pinTintColor = [UIColor purpleColor];
    
    [mapView addAnnotation:mapPin];
    [mapViewFullScreen addAnnotation:mapPin];   // for fullscreen map
}

#pragma mark - MapViewDelegate

-(MKAnnotationView *)mapView:(MKMapView *)mapView1 viewForAnnotation:(id<MKAnnotation>)annotation
{
//    MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier: @"asdf"];
//    pin.annotation = annotation;
//    pin.pinTintColor = [UIColor purpleColor];
//    
//    return pin;
    
    static NSString *reuseId = @"currentloc";
    
    MKPinAnnotationView *annView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
    if (annotation == mapView.userLocation || annotation == mapViewFullScreen.userLocation) {
        return nil;
    }
    if (annView == nil)
    {
        annView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
        annView.animatesDrop = NO;
        annView.canShowCallout = YES;
        annView.calloutOffset = CGPointMake(-5, 5);
    }
    else
    {
        annView.annotation = annotation;
    }
    
    annView.pinColor = MKPinAnnotationColorPurple;
 
    return annView;

}

#pragma mark - Location Manager delegate
// Location Manager Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
//    NSLog(@"%@", [locations lastObject]);
}


#pragma mark - TableView Methods

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return venuesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListViewCell *cell = (ListViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.backgroundColor = [UIColor whiteColor];
//    cell.cellImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"demo_img_%d",indexPath.row+1]];
//    
//    if (indexPath.row >5) {
//        cell.cellImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"demo_img_2"]];
//    }
    
        [cell.cellImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"image"]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        
        cell.titleLbl.text = [NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"name"]];
        cell.addressLbl.text = [NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"address"]];
        cell.distanceLbl.text = [NSString stringWithFormat:@"< %dkm",[[[venuesArray objectAtIndex:indexPath.row]valueForKey:@"distance"] intValue]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [searchBar resignFirstResponder];
    [self performSegueWithIdentifier:@"DetailBistroVC" sender:indexPath];
}

#pragma mark - SearchBar methods

-(void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    if(searchText.length == 0)
    {
        [venuesArray removeAllObjects];
        pageOffset = 1;
        [self VenuesAPI];
        isSearchClicked = NO;
        
//        [searchBar resignFirstResponder];

        searchBar.showsCancelButton = NO;

        if (showFullMap) {
            [mapViewFullScreen setHidden:NO];
        }
        
        self.viewTopConstraint.constant = 0.0f;
        isFiltered = FALSE;
        
        [tblView reloadData];
    }
    else
    {
        isSearchClicked = YES;
        searchBar.showsCancelButton = YES;
        
//        [filteredTableData removeAllObjects];
        
        isFiltered = true;
        [mapViewFullScreen setHidden:YES];
        
        self.viewTopConstraint.constant = -mapView.frame.size.height;
        
//        filteredTableData = [[NSMutableArray alloc] init];
//        
//        int i=0;
//        for (NSString *str in [poolTypesArray valueForKey:@"pooltype"])
//        {
//            NSRange nameRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            NSRange descriptionRange = [str rangeOfString:searchText options:NSCaseInsensitiveSearch];
//            if(nameRange.location != NSNotFound || descriptionRange.location != NSNotFound)
//            {
//                
//                [filteredTableData addObject:[poolTypesArray objectAtIndex:i]];
//            }
//            i++;
//        }
    }
    
//    NSLog(@"filteredTableData == %@",filteredTableData);
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar1
{
    
    searchBar.showsCancelButton = NO;

    if (showFullMap) {
        [mapViewFullScreen setHidden:NO];
    }
    self.viewTopConstraint.constant = 0.0f;
    searchBar.text = @"";
    
    [searchBar resignFirstResponder];
    pageOffset  = 1;
    isSearchClicked = NO;

    [venuesArray removeAllObjects];
    [self VenuesAPI];   // When there is no search then show Default API
    

}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    searchBar.showsCancelButton = NO;
    isSearchClicked = YES;
    [searchBar resignFirstResponder];

    pageOffset = 1;
    [venuesArray removeAllObjects];
    
    [self searchAPI];
}

#pragma mark - SearchAPI

-(void)searchAPI
{
    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading...";
    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"latitude"]] forKey:@"latitude"];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"UserDetails"]valueForKey:@"longitude"]] forKey:@"longitude"];
    [Dict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
    [Dict setValue:[[[NSUserDefaults standardUserDefaults]valueForKey:@"UserDetails"]valueForKey:@"user_id"] forKey:@"user_id"];
    [Dict setValue:searchBar.text forKey:@"query"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/users/search/",BASEURL] withParam:Dict withCompletion:^(NSDictionary *response, BOOL success){
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            
            //            venuesArray = [response valueForKey:@"venues"];
            [venuesArray addObjectsFromArray:[response valueForKey:@"venues"]];
            
            //            DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            
            [mapView removeAnnotations:mapView.annotations];
            [mapViewFullScreen removeAnnotations:mapViewFullScreen.annotations];
            
            for(int i = 0; i < venuesArray.count; i++)
            {
                [self addPinWithTitle:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"name"]] AndLatitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"latitude"]] AndLongitude:[NSString stringWithFormat:@"%@",[[venuesArray objectAtIndex:i]valueForKey:@"longitude"]]];
            }
            
            NSLog(@"venues_count == %d",[[response valueForKey:@"venues_count"] intValue]);
            
            if ([[response valueForKey:@"venues_count"] intValue]%5 >= 0) {
                isMorePage=YES;
            }
            else
            {
                isMorePage=NO;
            }
            
            [tblView reloadData];
            isPageRefresing=NO;
            
            if (venuesArray.count == 0) {
                DisplayAlertControllerWithTitle(@"No data found", @"Q'd");
            }
        }
        else
        {
            
        }
    }];

}

-(void)viewWillDisappear:(BOOL)animated
{
    pageOffset = 1;
    [searchBar resignFirstResponder];
}

-(IBAction)mapViewBtnClick:(id)sender
{
    
    [searchBar resignFirstResponder];
    searchBar.showsCancelButton = NO;

    self.viewTopConstraint.constant = 0.0f;
    searchBar.text = @"";

    UIButton *btn = (UIButton*)sender;
    
    if (btn.isSelected) {
        
        isSearchClicked = NO;
        pageOffset = 1;
        [venuesArray removeAllObjects];
        [self VenuesAPI];
        
        [mapViewBtn setTitle:@"MAP VIEW" forState:UIControlStateNormal];
        [mapViewFullScreen setHidden:YES];
        
        showFullMap = NO;
        [btn setSelected:NO];
    }
    else
    {
        [mapViewBtn setTitle:@"LIST VIEW" forState:UIControlStateNormal];
        
        showFullMap = YES;
        
        [mapViewFullScreen setHidden:NO];
        [btn setSelected:YES];
    }
}

#pragma mark SideButton CLick
-(IBAction)sideBtnClk:(id)sender
{
    UIButton *btn = (UIButton*)sender;
    if (btn.isSelected) {
        [btn setSelected:NO];
    }
    else
    {
        [btn setSelected:YES];
    }
    [searchBar resignFirstResponder];
    searchBar.text = @"";
    searchBar.showsCancelButton = NO;

    [self.mm_drawerController toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
    
}

#pragma mark Notification Center
-(void)showFadeImg:(NSNotification*)notification
{
    [fadeImgView setHidden:NO];
}

-(void)hideFadeImg:(NSNotification*)notification
{
    [fadeImgView setHidden:YES];
    [self.view bringSubviewToFront:tblView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ShowFadeImg" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"HideFadeImg" object:nil];
}

#pragma mark - ScrollView Delegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    float bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height;
    if (bottomEdge >= scrollView.contentSize.height)
    {
        if (!isPageRefresing)
        {
            isPageRefresing=YES;
            pageOffset= pageOffset+1;
            if (isMorePage)
            {
                if (isSearchClicked) {
                    [self searchAPI];
                }
                else
                {
                    [self VenuesAPI];
                }
            }
            
        }
        // we are at the end
    }

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    DetailBistroVC *VC = (DetailBistroVC*)[segue destinationViewController];
    VC.restaurantDetailDict = [venuesArray objectAtIndex:indexPath.row];
}

@end
