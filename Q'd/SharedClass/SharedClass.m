//
//  SharedClass.m
//  Qd
//
//  Created by SOTSYS028 on 12/01/16.
//  Copyright © 2016 SOTSYS028. All rights reserved.
//

#import "SharedClass.h"

static SharedClass *objSharedClass = nil;

@implementation SharedClass

+(SharedClass *)objSharedClass
{
    @synchronized(self)
    {
        if (objSharedClass == nil)
        {
            objSharedClass = [[SharedClass alloc] init];
        }
    }
    return objSharedClass;
}

@end
