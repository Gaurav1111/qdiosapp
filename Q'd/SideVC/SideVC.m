//
//  SideVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SideVC.h"
#import "SettingsVC.h"
#import "ViewController.h"

@interface SideVC ()

@end

@implementation SideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}


#pragma mark TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
//    cell.backgroundColor = [UIColor whiteColor];

    cell.textLabel.textColor = [UIColor darkGrayColor];
    //    cell.cellImgView.image = [UIImage imageNamed:@""];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Notification settings";
        cell.imageView.image = [UIImage imageNamed:@"Settings"];
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Sign out";
        cell.imageView.image = [UIImage imageNamed:@"Logout"];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Hind-Regular" size:15.0f];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"SettingsVC" sender:nil];     // When Click on Notifications
    }
    else if (indexPath.row == 1){
        
        for (UIViewController *VC in self.navigationController.viewControllers) {
            if ([VC isKindOfClass:[ViewController class]]) {
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"HasLogin"]; //When Logout is done
                
                DisplayAlertWithTitle(@"User logout successfully", @"Q'd");
                [self.navigationController popToViewController:VC animated:YES];
                return;
            }
        }
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"HasLogin"];
        [self performSegueWithIdentifier:@"GotoMainVC" sender:nil];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"GotoMainVC"]) {
        ViewController *VC = (ViewController*)[segue destinationViewController];
    }
    else
    {
        SettingsVC *VC = (SettingsVC*)[segue destinationViewController];
    }
}


@end
