//
//  SignUpVC.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SignUpVC.h"
#import "confirmationCodeVC.h"
#import "Common.h"
#import "MBProgressHUD.h"
#import "DrawerVC.h"

@interface SignUpVC ()
{
    CLLocationManager *locationManager;
    float Lat,Long;
    
    BOOL isFirstTime;
}
@property (weak, nonatomic) IBOutlet UITextField *emailTxtField;
@property (weak, nonatomic) IBOutlet UITextField *phnNoTxtField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxtField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPwdTxtField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxtField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxtField;

@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isFirstTime = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locationManager.delegate = self;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    CLLocation *location = [locationManager location];
    // Configure the new event with information from the location
    CLLocationCoordinate2D coordinate = [location coordinate];
    Lat = coordinate.latitude;
    Long = coordinate.longitude;
    
//    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
//    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
//    NSLog(@"dLatitude : %@", latitude);
//    NSLog(@"dLongitude : %@",longitude);
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

-(IBAction)backBtnClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)createAccountBtnClk:(id)sender
{
    
    if (([self.firstNameTxtField.text isEqualToString:@""] || [self.lastNameTxtField.text isEqualToString:@""] || [self.pwdTxtField.text isEqualToString:@""] || [self.confirmPwdTxtField.text isEqualToString:@""] || [self.emailTxtField.text isEqualToString:@""] || [self.phnNoTxtField.text isEqualToString:@""]) && isFirstTime == YES) {
        DisplayAlertControllerWithTitle(@"All Fields are mandetory", @"Q'd");
        return;
    }
    else
    {
        BOOL isValidEmail = [Common validateEmail:self.emailTxtField.text];
        
        if (!isValidEmail){
            DisplayAlertControllerWithTitle(@"Enter your valid email address", @"Q'd");
            return;
        }
        else if (self.pwdTxtField.text.length < 6 || self.confirmPwdTxtField.text.length < 6)
        {
            DisplayAlertControllerWithTitle(@"Password must contain minimum 6 characters", @"Q'd");
            return;
        }

        else if (![self.pwdTxtField.text isEqualToString:self.confirmPwdTxtField.text])
        {
            DisplayAlertControllerWithTitle(@"Password and Confirm Password not matched", @"Q'd");
        }
        else
        {
            isFirstTime = NO;
            
            MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Loading...";
            
            NSMutableDictionary *Dict = [NSMutableDictionary new];
            [Dict setValue:self.firstNameTxtField.text forKey:@"first_name"];
            [Dict setValue:self.lastNameTxtField.text forKey:@"last_name"];
            [Dict setValue:self.emailTxtField.text forKey:@"email"];
            [Dict setValue:self.pwdTxtField.text forKey:@"password"];
            [Dict setValue:self.phnNoTxtField.text forKey:@"phone"];
            [Dict setValue:self.confirmPwdTxtField.text forKey:@"password_confirmation"];
            [Dict setValue:[NSString stringWithFormat:@"%f",Lat] forKey:@"latitude"];
            [Dict setValue:[NSString stringWithFormat:@"%f",Long] forKey:@"longitude"];
            [Dict setValue:@"1" forKey:@"device_type"];
            [Dict setValue:XGET_VALUE(@"DeviceToken") forKey:@"device_token"];
            
            NSMutableDictionary *mainDict = [NSMutableDictionary new];
            [mainDict setObject:Dict forKey:@"user"];
            
            [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/signup",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                if (success)
                {
                    NSLog(@"response in success = %@",response);
                    [[NSUserDefaults standardUserDefaults]setObject:[response valueForKey:@"user"] forKey:@"UserDetails"];
                    
                    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLogin"];
                    [self performSegueWithIdentifier:@"DrawerVC" sender:nil];

//                    [self performSegueWithIdentifier:@"confirmationCodeVC" sender:nil];
                    DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                }
                else
                {
                    NSString *responseMessage = [response valueForKey:@"message"];
                    DisplayAlertControllerWithTitle(responseMessage, @"Q'd");
                }
            }];

        }
       
    }
}

#pragma mark TextField Delegates
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.emailTxtField) {
        
      BOOL isValidEmail = [Common validateEmail:self.emailTxtField.text];
    
        if (isValidEmail){
            
        }
        else{
            
        }
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.emailTxtField) {
        
        [self.phnNoTxtField becomeFirstResponder];
    }
    else if (textField == self.phnNoTxtField)
    {
        [self.pwdTxtField becomeFirstResponder];
    }
    else if (textField == self.pwdTxtField)
    {
        [self.confirmPwdTxtField becomeFirstResponder];
    }
    else if (textField == self.confirmPwdTxtField)
    {
        [textField resignFirstResponder];
    }
    else if (textField == self.firstNameTxtField)
    {
        [self.lastNameTxtField becomeFirstResponder];
    }
    else if (textField == self.lastNameTxtField)
    {
        [self.emailTxtField becomeFirstResponder];
    }
    
    
    
//    [textField resignFirstResponder];

    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"confirmationCodeVC"]) {
        confirmationCodeVC *VC = (confirmationCodeVC*)[segue destinationViewController];
    }
    else if ([segue.identifier isEqualToString:@"DrawerVC"]) {
        DrawerVC *VC = (DrawerVC*)[segue destinationViewController];
    }
}


@end
