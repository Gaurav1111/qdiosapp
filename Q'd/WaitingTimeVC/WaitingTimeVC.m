//
//  WaitingTimeVC.m
//  Qd
//
//  Created by SOTSYS028 on 15/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "WaitingTimeVC.h"

@interface WaitingTimeVC ()

@end

@implementation WaitingTimeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)yesBtnClick:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
